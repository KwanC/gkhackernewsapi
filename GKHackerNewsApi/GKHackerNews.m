//
//  GK_HN.m
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/18/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "GKHackerNews.h"

#define BASE_URL @"https://hacker-news.firebaseio.com/v0/"
#define MAX_INITIAL_ARTICLES_DOWNLOAD 20

NSString *const itemsURlStr = BASE_URL @"item";
NSString *const usersURLStr = BASE_URL @"user.json";
NSString *const maxItemUrlStr = BASE_URL @"maxitem.json";
NSString *const topStoriesUrlStr = BASE_URL @"topstories.json";
NSString *const newStoriesUrlStr = BASE_URL @"newstories.json";
NSString *const bestStoriesUrlStr = BASE_URL @"beststories.json";
NSString *const askStoriesUrlStr = BASE_URL @"askstories.json";
NSString *const showStoriesUrlStr = BASE_URL @"showstories.json";
NSString *const jobStoriesUrlStr = BASE_URL @"jobstories.json";
NSString *const updatesUrlStr = BASE_URL @"updates.json";

@interface GKHackerNews () {
    NSArray* _topStoriesList;
    NSInteger maxArticlesToDownload;
}
@end

@implementation GKHackerNews
-(id) init {
    self = [super init];
    if (self ) {
        maxArticlesToDownload = MAX_INITIAL_ARTICLES_DOWNLOAD;
    }
    return self;
}

-(void) initSDK {
    // get top stories
    [self topStories];
}

-(void) showHackerNews {
    NSBundle* mainBundle = [NSBundle mainBundle];
    NSURL* resBundleUrl = [mainBundle URLForResource:@"GKHackerNewsApiResources" withExtension:@"bundle"];    
    NSBundle *resBundle = [NSBundle bundleWithURL:resBundleUrl];
    
    GKHackNewListViewController* gkhnlvc = [[GKHackNewListViewController alloc]initWithNibName:@"GKHackNewListViewController" bundle:resBundle];
    gkhnlvc.topStoriesList = _topStoriesList;
    
    gkhnlvc.modalPresentationStyle = UIModalPresentationFormSheet;

    UINavigationController *navVc = [[UINavigationController alloc]initWithRootViewController:gkhnlvc];
    
    [[[[UIApplication sharedApplication]keyWindow]rootViewController] presentViewController:navVc animated:YES completion:nil];
}

- (void) topStories {
    NSURL *url = [[NSURL alloc]initWithString:topStoriesUrlStr];

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *aSession = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask* task = [aSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if ([self loggedResponse:response WithError:error]) { return; }
        // TODO: Update delegate to receive errors from loggedResponse
        
        NSError* parseError = nil;
        NSArray* topStoriesList = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];

        if (parseError != nil) {
            NSString* responseString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"Failed parsing top stories response.  Returned Data [%@]", responseString);
            return;
        }
        
        NSMutableArray* tTopStoriesList = [NSMutableArray array];
        NSURL* itemsBaseUrl = [NSURL URLWithString:itemsURlStr];
        
        for (NSNumber* articleId in topStoriesList) {
            GKArticle *article = [[GKArticle alloc] initWithArticleId:articleId BaseUrl:itemsBaseUrl];
            [tTopStoriesList addObject:article];
        }
        _topStoriesList = tTopStoriesList;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [_delegate topStoriesListAvailable];
        }];
    }];
    
    [task resume];
}

- (BOOL) loggedResponse:(NSURLResponse*) response WithError:(NSError*) error {
    BOOL retVal = NO;
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    if (httpResponse == nil) {
        NSLog(@"Failed Casting Response - %@", response);
        retVal = YES;
    } else {
        if (httpResponse.statusCode != 200) {
            NSString *msg = [NSString stringWithFormat:@"GKHackerNews - Url[%@] StatusCode[%ld]", httpResponse.URL.absoluteString, (long)httpResponse.statusCode];
            if (error != nil ) {
                msg = [NSString stringWithFormat:@"%@ Error[%@]", msg, error.localizedDescription];
            }
            NSLog(@"%@", msg);
            retVal = YES;
        }
    }
    
    return retVal;
}
@end

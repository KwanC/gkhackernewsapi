//
//  GKWebViewController.m
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/19/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "GKWebViewController.h"

@interface GKWebViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation GKWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_webView loadRequest:[NSURLRequest requestWithURL:_url]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  GKHackNewListViewController.m
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/18/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "GKHackNewListViewController.h"
#import "GKWebViewController.h"
#import "GKArticleLoadingTableViewCell.h"
#import "GKArticleTableViewCell.h"
#import "GKArticleFailedTableViewCell.h"

#define INITIAL_LIMIT 20

@interface GKHackNewListViewController () {
    __weak IBOutlet UITableView *articleTable;

    NSURLSession *session;
    NSOperationQueue *processQueue;
    NSInteger showLimit;
    NSBundle *resBundle;
}
@end

@implementation GKHackNewListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
    self.navigationItem.leftBarButtonItem = done;

    session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    processQueue = [[NSOperationQueue alloc]init];
    processQueue.name = @"Article Process Queue";
    
    NSBundle* mainBundle = [NSBundle mainBundle];
    NSURL* resBundleUrl = [mainBundle URLForResource:@"GKHackerNewsApiResources" withExtension:@"bundle"];
    resBundle = [NSBundle bundleWithURL:resBundleUrl];
    
    [articleTable registerNib:[UINib nibWithNibName:@"GKArticleLoadingTableViewCell" bundle:resBundle] forCellReuseIdentifier:@"loading"];
    [articleTable registerNib:[UINib nibWithNibName:@"GKArticleTableViewCell" bundle:resBundle] forCellReuseIdentifier:@"downloaded"];
    [articleTable registerNib:[UINib nibWithNibName:@"GKArticleFailedTableViewCell" bundle:resBundle] forCellReuseIdentifier:@"failed"];
    
    articleTable.delegate = self;
    articleTable.dataSource = self;
    articleTable.rowHeight = UITableViewAutomaticDimension;
    articleTable.estimatedRowHeight = 125;
    
    showLimit = INITIAL_LIMIT;
    if(_topStoriesList.count < showLimit) {
        showLimit = _topStoriesList.count;
    }
    
    [self fetchArticles];
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

-(void) fetchArticles {
    for(NSInteger articleIndex = 0; articleIndex < showLimit; articleIndex++) {
        GKArticle *article = (GKArticle*)[_topStoriesList objectAtIndex:articleIndex];
        NSURLSessionDataTask *task = [session dataTaskWithURL:article.url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if( error != nil) {
                return;
            }
            
            __block NSBlockOperation *processBlock = [NSBlockOperation blockOperationWithBlock:^{
                if(processBlock.cancelled) {
                    return;
                }
                ArticleState state = ArticleStateFailed;
                if (data.length > 0) {
                    NSError *parseError = nil;
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];
                    
                    if([article populatedWithJson:json]) {
                        state = ArticleStateDownloaded;
                    }
                }
                article.state = state;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    NSIndexPath *path = [NSIndexPath indexPathForRow:articleIndex inSection:0];
                    [articleTable reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
                }];
                
            }];
            
            [processQueue addOperation:processBlock];
        }];
        [task resume];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) doneButtonTapped:(id) sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [session invalidateAndCancel];
    [processQueue cancelAllOperations];
}

// MARK: Table View Stuff
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return showLimit;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GKArticle *article = (GKArticle*)[_topStoriesList objectAtIndex:indexPath.row];
    
    if(article.state == ArticleStateDownloaded) {
        NSLog(@"Loading %@", article.urlStr);
        if(article.urlStr != nil) {
            GKWebViewController *webViewController = [[GKWebViewController alloc]initWithNibName:@"GKWebViewController" bundle:resBundle];

            webViewController.url = [NSURL URLWithString:article.urlStr];
            [self.navigationController pushViewController:webViewController animated:YES];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* retCell;
    
    GKArticle *article = (GKArticle*)[_topStoriesList objectAtIndex:indexPath.row];
    
    switch (article.state) {
        case ArticleStateDownloaded :{
            GKArticleTableViewCell *cell = (GKArticleTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"downloaded"];
            [cell setArticle:article];
            retCell = cell;
            break;
        }
        case ArticleStateFailed :{
            GKArticleFailedTableViewCell *cell = (GKArticleFailedTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"failed"];
            retCell = cell;
            break;
        }
        case ArticleStateNew : {
            GKArticleLoadingTableViewCell *cell = (GKArticleLoadingTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"loading"];
            [cell.activityIndicator startAnimating];
            retCell = cell;
            break;
        }
    }
    
    return retCell;
}

@end

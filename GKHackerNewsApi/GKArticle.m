//
//  Article.m
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/18/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "GKArticle.h"

@implementation GKArticle
-(id) initWithArticleId:(NSNumber*)articleId BaseUrl:(NSURL*) baseUrl {
    self = [super init];
    
    if (self) {
        _state = ArticleStateNew;
        _id = articleId;
        _url = [baseUrl URLByAppendingPathComponent:[NSString stringWithFormat:@"%ld.json", (long)articleId.integerValue]];
    }
    return self;
}

-(BOOL) populatedWithJson:(NSDictionary*)json {
    BOOL retVal = NO;
    if (json!=nil) {
        NSNumber *tId = [json valueForKey:@"id"];
        if ([tId isEqualToNumber:_id]) {
            _id = tId;
            _deleted = (NSNumber*)[json valueForKey:@"deleted"];
            _type = (NSString*)[json valueForKey:@"type"];
            _by = (NSString*)[json valueForKey:@"by"];
            _time = (NSNumber*)[json valueForKey:@"time"];
            _text = (NSString*)[json valueForKey:@"text"];
            _dead = (NSNumber*)[json valueForKey:@"dead"];
            _parent = (NSNumber*)[json valueForKey:@"parent"];
            _kids = (NSArray*)[json valueForKey:@"kids"];
            _urlStr = (NSString*)[json valueForKey:@"url"];
            _score = (NSNumber*)[json valueForKey:@"score"];
            _title = (NSString*)[json valueForKey:@"title"];
            _parts = (NSArray*)[json valueForKey:@"parts"];
            _descendants = (NSNumber*)[json valueForKey:@"descendants"];
            retVal = YES;
        }
    }
    return retVal;
}
@end

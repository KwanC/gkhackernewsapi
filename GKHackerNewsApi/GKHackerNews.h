//
//  GK_HN.h
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/18/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKHackNewListViewController.h"
#import "GKArticle.h"

@protocol GKHackerNewsDelegate
-(void) topStoriesListAvailable;
@end

@interface GKHackerNews : NSObject
@property (weak) id<GKHackerNewsDelegate> delegate;

-(void) initSDK;
-(void) showHackerNews;
@end

//
//  Article.h
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/18/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ArticleState) {
    ArticleStateNew,
    ArticleStateDownloaded,
    ArticleStateFailed
};

@interface GKArticle : NSObject
@property ArticleState state;
@property NSURL *url;

@property NSNumber* id;
@property NSNumber* deleted;
@property NSString* type;
@property NSString* by;
@property NSNumber* time;
@property NSString* text;
@property NSNumber* dead;
@property NSNumber* parent;
@property NSArray*  kids;
@property NSString* urlStr;
@property NSNumber* score;
@property NSString* title;
@property NSArray*  parts;
@property NSNumber* descendants;

-(id) initWithArticleId:(NSNumber*)articleId BaseUrl:(NSURL*) baseUrl;
-(BOOL) populatedWithJson:(NSDictionary*)json;
@end

//
//  GKArticleTableViewCell.m
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/19/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "GKArticleTableViewCell.h"

@interface GKArticleTableViewCell(){
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblInfo;
}
@end

@implementation GKArticleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setArticle:(GKArticle*)article {
    NSString *infoStr = [NSString stringWithFormat:@"%@ points by %@ | %@ comments", article.score, article.by, article.descendants];
    
    lblTitle.text = article.title;
    lblInfo.text = infoStr;
}
@end

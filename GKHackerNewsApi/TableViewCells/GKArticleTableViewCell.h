//
//  GKArticleTableViewCell.h
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/19/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKArticle.h"

@interface GKArticleTableViewCell : UITableViewCell
-(void) setArticle:(GKArticle*)article;
@end

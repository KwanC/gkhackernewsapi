//
//  GKArticleLoadingTableViewCell.h
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/19/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GKArticleLoadingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

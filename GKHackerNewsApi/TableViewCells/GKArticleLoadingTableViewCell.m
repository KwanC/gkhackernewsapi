//
//  GKArticleLoadingTableViewCell.m
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/19/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "GKArticleLoadingTableViewCell.h"

@implementation GKArticleLoadingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

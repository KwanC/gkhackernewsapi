//
//  GKWebViewController.h
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/19/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GKWebViewController : UIViewController
@property NSURL *url;
@end

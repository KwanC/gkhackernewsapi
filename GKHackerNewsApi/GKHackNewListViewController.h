//
//  GKHackNewListViewController.h
//  HackerNewsApi
//
//  Created by Kwan Cheng on 2/18/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKArticle.h"

@interface GKHackNewListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak) NSArray *topStoriesList;
@end
